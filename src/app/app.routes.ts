import { Routes } from '@angular/router';
import { UserPageComponent } from '../pages/user-page/user-page.component';
import { HomeComponent } from '../pages/home/home.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'user/:id',
    component: UserPageComponent,
  },
];
