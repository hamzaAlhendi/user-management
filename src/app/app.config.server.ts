import { mergeApplicationConfig, ApplicationConfig } from '@angular/core';

import { appConfig } from './app.config';
import { StoreModule } from '@ngrx/store';

const serverConfig: ApplicationConfig = {
  providers: [],
};

export const config = mergeApplicationConfig(appConfig, serverConfig);
