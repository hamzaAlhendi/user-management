import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState, userAdapter } from './user.reducer';

const { selectAll } = userAdapter.getSelectors();

export const selectUserState = createFeatureSelector<UserState>('users');

export const selectAllUsers = createSelector(selectUserState, selectAll );
