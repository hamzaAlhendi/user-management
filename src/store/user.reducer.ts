import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { User } from '../models/user';
import { addUser, removeUsers } from './user.actions';

export interface UserState extends EntityState<User> {}

export const userAdapter = createEntityAdapter<User>();

export const initialState: UserState = userAdapter.getInitialState();

export const userReducer = createReducer(
  initialState,
  on(addUser, (state, { user }) => {
    return userAdapter.addOne(user, state);
  }),
  on(removeUsers, (state) => {
    return userAdapter.removeAll(state);
  })
);
