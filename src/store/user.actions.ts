import { createAction, props } from '@ngrx/store';
import { User } from '../models/user';

// to add user
export const addUser = createAction('[User] Add User', props<{ user: User }>());

// to delete all users
export const removeUsers = createAction('[Users] Remove Users');
