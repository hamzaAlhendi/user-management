import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { User } from '../models/user';
import { Store } from '@ngrx/store';
import { addUser } from '../store/user.actions';

interface ApiResponse {
  data: User[];
}

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(
    private httpClient: HttpClient,
    private store: Store<{ user: User[] }>
  ) {}

  getUsers(page: number = 1) {
    return this.httpClient
      .get<ApiResponse>(`https://reqres.in/api/users?page=${page}`)
      .pipe(map((response) => response['data']))
      .subscribe((value) => {
        value.forEach((user) => {
          this.store.dispatch(addUser({ user }));
        });
      });
  }

  getUser(id: string) {
    return this.httpClient
      .get<{ data: User }>(`https://reqres.in/api/users/${id}`)
      .pipe(map((response) => response['data']));
  }
}
