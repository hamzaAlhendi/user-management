import { Component, Input } from '@angular/core';
import { MatCardModule } from '@angular/material/card';

import { UsersService } from '../../services/users.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-user-card',
  standalone: true,
  imports: [MatCardModule],
  templateUrl: './user-card.component.html',
  styleUrl: './user-card.component.css',
})
export class UserCardComponent {
  @Input() user!: User;
  constructor(private usersService: UsersService) {}
}
