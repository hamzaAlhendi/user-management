import { AfterViewInit, Component, ViewChild } from '@angular/core';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';

import {
  MatPaginator,
  MatPaginatorModule,
  PageEvent,
} from '@angular/material/paginator';
import { CommonModule } from '@angular/common';
import { UserCardComponent } from '../../components/user-card/user-card.component';
import { UsersService } from '../../services/users.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Observable, filter, map } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { selectAllUsers } from '../../store/user.selectors';
import { NgrxModule } from '../../ngrx/ngrx.module';
import { removeUsers } from '../../store/user.actions';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    UserCardComponent,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatGridListModule,
    HttpClientModule,
    MatCardModule,
    CommonModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    NgrxModule,
  ],
  providers: [UsersService],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  isLoading: boolean = true;
  users$: Observable<User[]> = new Observable();

  constructor(
    private usersService: UsersService,
    private router: Router,
    private store: Store<{ users: User[] }>
  ) {
    usersService.getUsers();
    this.users$ = this.store.pipe(
      select(selectAllUsers),
      map((usersDire) => Object.values(usersDire)),
      filter((users) => !!users)
    );
  }

  ngAfterViewInit() {
    this.paginator?.page.subscribe((pageEvent: PageEvent) => {
      // refresh the store before adding the new users page
      this.store.dispatch(removeUsers());
      this.usersService.getUsers(pageEvent.pageIndex + 1);
    });
  }

  navigateToUser(userId: string) {
    this.router.navigate([`user/${userId}`]);
  }
}
