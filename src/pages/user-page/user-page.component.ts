import { Component } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { UsersService } from '../../services/users.service';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCard, MatCardContent } from '@angular/material/card';
import { User } from '../../models/user';
import { CommonModule, Location } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgrxModule } from '../../ngrx/ngrx.module';

@Component({
  selector: 'app-user-page',
  standalone: true,
  imports: [
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatCard,
    MatCardContent,
    CommonModule,
    MatProgressSpinnerModule,
    NgrxModule,
  ],
  providers: [UsersService],
  templateUrl: './user-page.component.html',
  styleUrl: './user-page.component.css',
})
export class UserPageComponent {
  isLoading: boolean = true;
  user!: User;
  constructor(
    private usersService: UsersService,
    private router: Router,
    private location: Location
  ) {
    const id = this.router.url.slice(
      this.router.url.lastIndexOf('/') + 1,
      this.router.url.length
    );
    this.usersService.getUser(id).subscribe((value) => {
      this.user = value;
      setTimeout(() => {
        this.isLoading = false;
      }, 1000);
    });
  }

  goBack() {
    this.location.back();
  }
}
