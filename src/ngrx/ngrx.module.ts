import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { userReducer } from '../store/user.reducer';

@NgModule({
  declarations: [],
  imports: [CommonModule, StoreModule.forRoot({ users: userReducer })],
})
export class NgrxModule {}
